import { Flex, Heading, Button } from "@chakra-ui/react";
import React, { useState } from "react";
import Link from "next/link";
import { deleteToken } from "../api/helper";
import { useRouter } from "next/router";

const PUBLIC_ROUTE = ["/signup", "/login"];

export const Navbar = (props) => {
  const [hasLogout, setHasLogout] = useState(false);
  const router = useRouter();

  return (
    <Flex
      h="64px"
      position="fixed"
      bgColor="blue.900"
      alignItems="center"
      px={4}
      w="100%"
      justifyContent="space-between"
      zIndex="100"
    >
      <Link href="/dashboard">
        <Heading
          textStyle="bold"
          fontSize="xl"
          color="white"
          style={{ cursor: "pointer" }}
        >
          Sistem Informasi Tugas
        </Heading>
      </Link>

      <Flex
        h="9"
        position="fixed"
        alignItems="right"
        px={9}
        w="100%"
        justifyContent="flex-end">
        <Link href="/edit-matkul">
          <Button fontSize="14px;" mx={4} _hover={{
            background: "teal.500",
          }}>
            Pilih Matkul
          </Button>
        </Link>
        <Link href="/dashboard">
          <Button fontSize="14px;" mx={4} _hover={{
            background: "teal.500",
          }}>
            Dashboard
          </Button>
        </Link>
        {!PUBLIC_ROUTE.includes(router.pathname) && (
          <Link href="/login">
            <Button backgroundColor="red.400" mx={4} onClick={() => deleteToken()}>
              Logout
          </Button>
          </Link>
        )}
      </Flex>

    </Flex>
  );
};
