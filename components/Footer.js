import { Flex, Text } from "@chakra-ui/react";
import React from "react";


export const Footer = (props) => {
    const code = "</>";
    return (
        <Flex
            h="86px"
            bgColor="blue.900"
            alignItems="center"
            px={4}
            w="100%"
            justifyContent="space-between"
        >
            <Text color="white" mx="10" >
                {code} by Advprog C2
            </Text>
        </Flex>
    );
};
