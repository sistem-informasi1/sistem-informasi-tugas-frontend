import { VStack, Box, Text, Flex } from "@chakra-ui/react";
import React from "react";
import { FE_BASE_URL } from "../api/constant";
import Link from "next/link";

const detailTugas = (idString) => {
  window.location.replace(`${FE_BASE_URL}/matkul/tugas/` + idString);
};

const formatDate = (dateString) => {
  const options = { year: "numeric", month: "long", day: "numeric" };
  return new Date(dateString).toLocaleDateString(undefined, options);
};

const isExpired = (date) => {
  const now = new Date();
  const deadline = new Date(date);
  if (deadline > now) return false;
  return true;
};

export const BoxTugas = (props) => {
  return (
    <Box
      borderRadius="10px"
      shadow="xl"
      h="160px"
      w="253px"
      bgColor="teal.50"
      rounded="md"
      key={props.id}
    >
      <Text mt="12px" ml="24px" fontSize="20px" color="black">
        {" "}
        {props.matkul}
      </Text>
      <Text mt="12px" ml="24px" fontSize="16px" color="black">
        {" "}
        {props.judul}
      </Text>
      <VStack
        mt="12px"
        ml="24px"
        backgroundColor={`${isExpired(props.deadline) ? "red.600" : "teal.600"}`}
        h="26px"
        w="136px"
        borderRadius="5px"
      >
        <Text fontSize="14px;" color="white">
          {formatDate(props.deadline)}
        </Text>
      </VStack>
      <Flex justifyContent="flex-end">
        <Link href={`/tugas/${props.id}`}>
          <Text
            cursor="pointer"
            textDecor="underline"
            mr="24px"
            mt="2"
            color="teal.700"
          >
            Lihat Detail
          </Text>
        </Link>
      </Flex>
    </Box>
  );
};
