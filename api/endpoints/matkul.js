import axios from "axios";
import { BE_BASE_URL } from "../constant";
import { getToken } from "../helper";

export class MatkulAPI {
  async getAllMatkul() {
    return axios.get(`${BE_BASE_URL}/matkul`, {
      headers: { Authorization: "Bearer " + getToken() },
    });
  }
  async subscribeToMatkuls(data) {
    return axios.post(
      `${BE_BASE_URL}/matkul/subscribe`,
      { kodeMatkuls: data },
      { headers: { Authorization: "Bearer " + getToken() } }
    );
  }
  async createMatkul(data) {
    return axios.post(`${BE_BASE_URL}/matkul`, data, {
      headers: { Authorization: "Bearer " + getToken() },
    });
  }
}
