import axios from "axios";
import { BE_BASE_URL } from "../constant";
import { getToken } from "../helper";

export class TugasAPI {
  async getAllTugas() {
    return axios.get(`${BE_BASE_URL}/tugas`, {
      headers: { Authorization: "Bearer " + getToken() },
    });
  }

  async getDetailTugas(kode) {
    return axios.get(`${BE_BASE_URL}/tugas/` + kode, {
      headers: { Authorization: "Bearer " + getToken() },
    });
  }

  async createTugas(kodeMatkul, data) {
    return axios.post(`${BE_BASE_URL}/tugas/${kodeMatkul}`, data, {
      headers: { Authorization: "Bearer " + getToken() },
    });
  }
}
