import axios from "axios";
import { BE_BASE_URL } from "../constant";
import { getToken } from "../helper";

export class KomentarAPI {
  async createKomentar(kodeTugas, data) {
    return axios.post(`${BE_BASE_URL}/komentar/` + kodeTugas, data, {
      headers: { Authorization: "Bearer " + getToken() },
    });
  }

}