import React, { useState } from "react";
import {
  Center,
  Heading,
  FormControl,
  FormLabel,
  Input,
  Text,
  VStack,
  Button,
  FormHelperText,
} from "@chakra-ui/react";
import { Footer } from "../components/Footer";
import { AuthenticationAPI } from "../api/endpoints/authentication";
import { deleteToken, redirect, saveToken } from "../api/helper";
import { FE_BASE_URL } from "../api/constant";
import Link from "next/link"

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  const auth = new AuthenticationAPI();

  const onSubmit = () => {
    deleteToken();
    auth
      .login({ email: email, password: password })
      .then((res) => {
        // save token to local storage
        saveToken(res.data);
        auth.getUser()
          .then(res => {
            if (res.data.matkulList.length === 0) redirect("/edit-matkul")
            else redirect("/dashboard")
          })
      })
      .catch((err) => {
        if (err.response.status === 403) setError("Email/password salah");
      });
  };

  return (
    <>
      <Center backgroundColor="teal.400" minHeight="100vh">
        <VStack
          backgroundColor="teal.50"
          py="32px"
          px="100px"
          borderRadius="10px"
          spacing={8}
        >
          <Heading>Sign In</Heading>
          <FormControl id="email" isRequired>
            <FormLabel>Email</FormLabel>
            <Input
              backgroundColor="white"
              type="email"
              placeholder="dummy@dummy.com"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
            <FormHelperText color="red.500">{error}</FormHelperText>
          </FormControl>
          <FormControl id="password" isRequired>
            <FormLabel>Password</FormLabel>
            <Input
              backgroundColor="white"
              type="password"
              placeholder="Password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </FormControl>
          <Button
            backgroundColor="blue.900"
            color="white"
            onClick={() => onSubmit()}
          >
            Sign In
        </Button>
          <Text>Dont have have an account ? <Link href="/signup"><span style={{ textDecoration: "underline", cursor: "pointer" }}>Sign Up</span></Link></Text>
        </VStack>
      </Center>
      <Footer />
    </>
  );
};

export default Login;
