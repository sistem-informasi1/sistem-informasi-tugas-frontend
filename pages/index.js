import { Heading, HStack, VStack, Button } from "@chakra-ui/react";
import Head from "next/head";
import Link from "next/link";
import { Footer } from "../components/Footer";
import { useEffect } from "react";
import { AuthenticationAPI } from "../api/endpoints/authentication";
import { redirect } from "../api/helper";

export default function Home() {
  return (
    <div>
      <Head>
        <title>Sistem Informasi Tugas</title>
        <meta
          name="description"
          content="Sebuah aplikasi untuk melihat dan mendapatkan notifikasi tugas perkuliahan"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <VStack h="100vh" justifyContent="center" backgroundColor="teal.400">
          <Heading color="blue.900" mb={3}>
            Sistem Informasi Tugas
          </Heading>
          <HStack>
            <Link href="/login">
              <Button backgroundColor="blue.50" color="black">
                Login
              </Button>
            </Link>
            <Link href="/signup">
              <Button backgroundColor="blue.900" color="white">
                Signup
              </Button>
            </Link>
          </HStack>
        </VStack>
      </main>
      <Footer />
    </div>
  );
}
