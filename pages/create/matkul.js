import React, { useState } from "react";
import { redirect } from "../../api/helper";
import { Navbar } from "../../components/Navbar";
import { Footer } from "../../components/Footer";
import {
  VStack,
  Center,
  Heading,
  Select,
  FormControl,
  FormLabel,
  Input,
  Button,
  useToast,
  Spinner,
} from "@chakra-ui/react";
import { MatkulAPI } from "../../api/endpoints/matkul";

const Matkul = () => {
  const toast = useToast()
  const matkulApi = new MatkulAPI();
  const [data, setData] = useState({});
  const [loading, setLoading] = useState(false);

  const submitHandler = () => {
    setLoading(true);
    matkulApi
      .createMatkul(data)
      .then((res) => {
        setLoading(false);
        toast({
          title: "Matkul Created",
          description: "A new matkul is created successfully",
          status: "success",
          duration: 9000,
          isClosable: true,
        });
      })
      .catch((err) => {
        console.log(err);
        toast({
          title: "Failed to create matkul",
          status: "failed",
          duration: 9000,
          isClosable: true,
        });
        setLoading(false);
      });
  };

  return (
    <>
      <Navbar />
      <Center backgroundColor="teal.400" minHeight="100vh" pt="12">
        <VStack spacing="4">
          <Heading mb="4">Add Matkul</Heading>
          <FormControl>
            <FormLabel>Kode Matkul</FormLabel>
            <Input
              isRequired
              value={data.kodeMatkul}
              onChange={(e) => {
                setData({ ...data, kodeMatkul: e.target.value });
              }}
              bg="white"
              type="text"
            />
          </FormControl>
          <FormControl>
            <FormLabel>Nama Matkul</FormLabel>
            <Input
              isRequired
              value={data.nama}
              onChange={(e) => {
                setData({ ...data, nama: e.target.value });
              }}
              bg="white"
              type="text"
            />
          </FormControl>
          <Button onClick={() => submitHandler()} bg="blue.600" color="white">
            {loading ? <Spinner size="xl" /> : "Add Matkul"}
          </Button>
        </VStack>
      </Center>
      <Footer />
    </>
  );
};

export default Matkul;
