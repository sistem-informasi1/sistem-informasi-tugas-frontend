import {
  Center,
  VStack,
  Input,
  Heading,
  Flex,
  Button,
  Checkbox,
  Box,
} from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
import { FE_BASE_URL } from "../api/constant";
import { AuthenticationAPI } from "../api/endpoints/authentication";
import { MatkulAPI } from "../api/endpoints/matkul";
import { redirect, redirectToLogin } from "../api/helper";
import { Navbar } from "../components/Navbar";
import { Footer } from "../components/Footer";
import Link from "next/link";

const EditMatkul = () => {
  const matkulApi = new MatkulAPI();
  const authApi = new AuthenticationAPI();
  const [matkuls, setMatkuls] = useState([]);
  const [matkulCheckeds, setMatkulCheckeds] = useState([]);
  const [filteredMatkuls, setFilteredMatkuls] = useState([]);

  useEffect(() => {
    matkulApi
      .getAllMatkul()
      .then((res) => {
        setMatkuls(res.data);
        setFilteredMatkuls(res.data);
      })
      .catch((err) => {
        if (err.response.status === 403) redirectToLogin();
      });
    authApi
      .getUser()
      .then((res) => setMatkulCheckeds(res.data.matkulList))
      .catch((err) => {
        if (err.response.status === 403) redirectToLogin();
      });
  }, []); // every time the page loaded

  const handleCheckMatkul = (kodeMatkul) => {
    if (matkulCheckeds.includes(kodeMatkul)) {
      setMatkulCheckeds(matkulCheckeds.filter((el) => el !== kodeMatkul));
    } else {
      setMatkulCheckeds([...matkulCheckeds, kodeMatkul]);
    }
  };

  const handleSimpanMatkuls = () => {
    matkulApi
      .subscribeToMatkuls(matkulCheckeds)
      .then((res) => {
        redirect("/dashboard");
      })
      .catch((err) => {
        if (err.response.status === 403) redirectToLogin();
      });
  };

  const handleSearchMatkul = (event) => {
    setFilteredMatkuls(
      matkuls.filter(
        (el) =>
          el.nama.toLowerCase().indexOf(event.target.value.toLowerCase()) !== -1
      )
    );
  };

  return (
    <>
      <Navbar />
      <Center pt={24} bgColor="teal.400" minH="100vh">
        <VStack w={{ xl: "24rem" }}>
          <VStack w="100%">
            <VStack mb={4} w="100%">
              <Heading textAlign="left" color="blue.900" fontSize="4xl">
                Pilih Mata Kuliah
              </Heading>
              <Input
                type="text"
                placeholder="Cari mata kuliah berdasarkan nama"
                bgColor="white"
                onChange={handleSearchMatkul}
              />
            </VStack>
          </VStack>
          <VStack maxH="300px" overflowY="auto" w="100%">
            {filteredMatkuls.map((element) => (
              <Box
                w="100%"
                py="14px"
                px="18px"
                bgColor="green.50"
                key={element.kodeMatkul}
              >
                <Checkbox
                  isChecked={matkulCheckeds.includes(element.kodeMatkul)}
                  onChange={() => handleCheckMatkul(element.kodeMatkul)}
                >
                  {element.nama}
                </Checkbox>
              </Box>
            ))}
          </VStack>
          <Button
            bgColor="blue.900"
            color="white"
            w="100%"
            onClick={() => handleSimpanMatkuls()}
            mb="8"
          >
            Simpan
          </Button>
          <Link href="/create/matkul">
            <Button bgColor="blue.900" color="white" w="100%">
              Add Matkul
            </Button>
          </Link>
        </VStack>
      </Center>
      <Footer />
    </>
  );
};

export default EditMatkul;
